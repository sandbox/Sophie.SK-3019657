<?php

namespace Drupal\commerce_attachments\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines the list builder for product attachments.
 *
 * @ingroup commerce_attachments
 */
class ProductAttachmentListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['optional'] = $this->t('Optional?');
    $header['status'] = $this->t('Status');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\commerce_attachments\Entity\ProductAttachmentInterface $entity */
    $row['label']['data'] = [
      '#type' => 'link',
      '#title' => $entity->getLabel(),
    ] + $entity->toUrl()->toRenderArray();
    $row['optional'] = $entity->isOptional() ? $this->t('Yes') : $this->t('No');
    $row['status'] = $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished');

    return $row + parent::buildRow($entity);
  }

}
