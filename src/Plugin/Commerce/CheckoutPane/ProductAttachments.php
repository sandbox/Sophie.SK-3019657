<?php

namespace Drupal\commerce_attachments\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;

/**
 * A checkout pane allowing customers to select product attachments.
 *
 * @CommerceCheckoutPane(
 *   id = "product_attachments",
 *   label = @Translation("Product attachments"),
 *   default_step = "order_information",
 * )
 */
class ProductAttachments extends CheckoutPaneBase {}
