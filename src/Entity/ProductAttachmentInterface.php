<?php

namespace Drupal\commerce_attachments\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for product attachment entities.
 *
 * @ingroup commerce_attachments
 */
interface ProductAttachmentInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Get the 'optional' value for an entity.
   *
   * @return bool
   *   Whether the entity is an optional attachment.
   */
  public function isOptional(): bool;

  /**
   * Get the 'send' text for an entity.
   *
   * @return string
   *   The 'send' text value.
   */
  public function getSendText(): string;

  /**
   * Get the 'do not send' text for an entity.
   *
   * @return string
   *   The 'do not send' text value.
   */
  public function getDoNotSendText(): string;

}
