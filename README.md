INTRODUCTION
------------

This module provides the **Product attachment** entity.

A product attachment can be added to a Commerce product. Site builders will be
able to add mark attachments as optional, and during checkout, users will be
able to choose whether they should get that attachment.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_attachments

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/commerce_attachments


REQUIREMENTS
------------

You must have the Commerce module installed. For more information, see:
https://www.drupal.org/project/commerce


RECOMMENDED MODULES
-------------------

 * There are no recommended modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

 * This module provides a content entity type, Product Attachment. It can be
   configured, and fields added/removed/changed, under Commerce > Product
   Attachments.

 * If you have the "default" product type installed, you should find an entity
   reference field on it, where attachments can be added or removed from a
   product.

 * stuff then happens.


MAINTAINERS
-----------

Current maintainers:
 * Sophie Shanahan-Kluth (Sophie.SK) - https://drupal.org/user/1540896

This project has been sponsored by:
 * Microserve Ltd
   Microserve has been creating brilliant Drupal websites for companies from
   financial services to non-profits, NGOs and local government since 2004.
   Visit https://microserve.io/ for more information.
